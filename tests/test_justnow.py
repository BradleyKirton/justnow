from __future__ import annotations

import datetime

import pytest
from justnow.parser import (
    INFINITY,
    EventParser,
    TimeEvent,
    get_elapse_datetime,
    get_next_elapse_datetime,
    get_specification_ast,
    parse_date_section,
    parse_datepart_token,
    parse_time_event,
    parse_time_section,
    parse_weekday_section,
    walk_days,
    walk_hours,
    walk_minutes,
    walk_months,
    walk_seconds,
)
from lark.lexer import Token
from lark.tree import Tree


@pytest.fixture(name="specification_ast")
def fixture_specification_ast() -> Tree:
    return get_specification_ast("tue 2020-08-11")


class TestParseDateSection:
    def test_single(self) -> None:
        specification_ast = get_specification_ast("2020-08-11")
        date_parts = parse_date_section(specification_ast)
        assert date_parts == ({2020}, {8}, {11})

    def test_composite(self) -> None:
        specification_ast = get_specification_ast("2020,2021-08-11")
        date_parts = parse_date_section(specification_ast)
        assert date_parts == ({2020, 2021}, {8}, {11})


class TestParseTimeSection:
    def test_single(self) -> None:
        specification_ast = get_specification_ast("tue 2020-08-11")
        time_parts = parse_time_section(specification_ast)
        assert time_parts == ({0}, {0}, {0})

    def test_composite(self) -> None:
        specification_ast = get_specification_ast("00,12:00:00")
        time_parts = parse_time_section(specification_ast)
        assert time_parts == ({0, 12}, {0}, {0})


def test_parse_weekday_section(specification_ast: Tree) -> None:
    weekdays = set(parse_weekday_section(specification_ast))
    assert weekdays == set([1])


def test_parse_datepart_token_wildcard() -> None:
    token = Token("year", "*")
    assert parse_datepart_token(token=token) == INFINITY


def test_parse_datepart_token_int() -> None:
    token = Token("year", "2010")
    assert parse_datepart_token(token=token) == 2010


def test_get_specification_ast() -> None:
    get_specification_ast("tue 2020-08-11")


def test_parse_time_event(specification_ast: Tree) -> None:
    time_event = parse_time_event(specification_ast)
    actual_result = time_event._asdict()
    assert actual_result == {
        "years": {2020},
        "months": {8},
        "days": {11},
        "hours": {0},
        "minutes": {0},
        "seconds": {0},
        "weekdays": set([1]),
    }


class TestGetNextElapseDatetime:
    def test_get_next_elapse_datetime(self, specification_ast: Tree) -> None:
        time_event = parse_time_event(specification_ast)
        reference_date = datetime.datetime(2020, 8, 1)
        date_gen = get_next_elapse_datetime(
            years=time_event.years,
            months=time_event.months,
            days=time_event.days,
            hours=time_event.hours,
            minutes=time_event.minutes,
            seconds=time_event.seconds,
            weekdays=time_event.weekdays,
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 8, 11)

    def test_get_next_elapse_datetime_past_year(self) -> None:
        specification_ast = get_specification_ast("2010-*-*")
        time_event = parse_time_event(specification_ast)
        reference_date = datetime.datetime(2020, 8, 1)
        date_gen = get_next_elapse_datetime(
            years=time_event.years,
            months=time_event.months,
            days=time_event.days,
            hours=time_event.hours,
            minutes=time_event.minutes,
            seconds=time_event.seconds,
            weekdays=time_event.weekdays,
            reference_date=reference_date,
        )
        assert next(date_gen, None) is None

    def test_non_leap_year(self) -> None:
        specification_ast = get_specification_ast("2019-02-29")
        time_event = parse_time_event(specification_ast)
        reference_date = datetime.datetime(2019, 1, 1)
        date_gen = get_next_elapse_datetime(
            years=time_event.years,
            months=time_event.months,
            days=time_event.days,
            hours=time_event.hours,
            minutes=time_event.minutes,
            seconds=time_event.seconds,
            weekdays=time_event.weekdays,
            reference_date=reference_date,
        )
        assert next(date_gen, None) is None


class TestNamedEvents:
    def test_minutely(self) -> None:
        specification_ast = get_specification_ast("@minutely")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={INFINITY},
            days={INFINITY},
            hours={INFINITY},
            minutes={INFINITY},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result

    def test_hourly(self) -> None:
        specification_ast = get_specification_ast("@hourly")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={INFINITY},
            days={INFINITY},
            hours={INFINITY},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result

    def test_daily(self) -> None:
        specification_ast = get_specification_ast("@daily")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={INFINITY},
            days={INFINITY},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result

    def test_monthly(self) -> None:
        specification_ast = get_specification_ast("@monthly")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={INFINITY},
            days={1},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result

    def test_weekly(self) -> None:
        specification_ast = get_specification_ast("@weekly")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={INFINITY},
            days={INFINITY},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0},
        )
        assert actual_result == expected_result

    def test_yearly(self) -> None:
        specification_ast = get_specification_ast("@yearly")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={1},
            days={1},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result

    def test_quarterly(self) -> None:
        specification_ast = get_specification_ast("@quarterly")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={1, 4, 7, 10},
            days={1},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result

    def test_semiannually(self) -> None:
        specification_ast = get_specification_ast("@semiannually")
        actual_result = parse_time_event(specification_ast)
        assert actual_result == TimeEvent(
            years={INFINITY},
            months={1, 7},
            days={1},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )

    def test_annually(self) -> None:
        specification_ast = get_specification_ast("@annually")
        actual_result = parse_time_event(specification_ast)
        expected_result = TimeEvent(
            years={INFINITY},
            months={1},
            days={1},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
        )
        assert actual_result == expected_result


def test_get_elapse_datetime() -> None:
    reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
    elapse_datetime = get_elapse_datetime("fri", reference_date=reference_date)
    assert elapse_datetime == datetime.datetime(2020, 1, 3, 0, 0, 0)


class TestParser:
    def test__next__(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        parser = EventParser("fri", reference_date=reference_date)
        assert next(parser) == datetime.datetime(2020, 1, 3, 0, 0, 0)

    def test_get_next_n(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        parser = EventParser("fri", reference_date=reference_date)
        assert list(parser.get_next_n(limit=2)) == [
            datetime.datetime(2020, 1, 3, 0, 0, 0),
            datetime.datetime(2020, 1, 10, 0, 0, 0),
        ]

    def test__iter__(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        parser = EventParser("fri", reference_date=reference_date)
        assert iter(parser) == parser._date_gen

    def test__leap_year(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        parser = EventParser("sat *-02-29", reference_date)
        assert next(parser) == datetime.datetime(2020, 2, 29, 0, 0)


def test_walk_months() -> None:
    reference_date = datetime.datetime(2019, 1, 1, 0, 0, 0)
    date_gen = walk_months(
        year=2020,
        months={INFINITY},
        days={1},
        hours={0},
        minutes={0},
        seconds={0},
        weekdays={0, 1, 2, 3, 4, 5, 6},
        reference_date=reference_date,
    )
    assert next(date_gen) == datetime.datetime(2020, 1, 1, 0, 0, 0)


class TestWalkDays:
    def test_start_from_reference(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_days(
            year=2020,
            month=1,
            days={INFINITY},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 1, 0, 0, 0)

    def test_iter_all_days(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_days(
            year=2020,
            month=2,
            days={INFINITY},
            hours={0},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 2, 1, 0, 0, 0)


class TestWalkHours:
    def test_start_from_reference(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_hours(
            year=2020,
            month=1,
            day=1,
            hours={INFINITY},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 1, 0, 0, 0)

    def test_iter_all_hours(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_hours(
            year=2020,
            month=1,
            day=2,
            hours={INFINITY},
            minutes={0},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 2, 0, 0, 0)


class TestWalkMinutes:
    def test_start_from_reference(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_minutes(
            year=2020,
            month=1,
            day=1,
            hour=0,
            minutes={INFINITY},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 1, 0, 0, 0)

    def test_iter_all_minutes(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_minutes(
            year=2020,
            month=1,
            day=2,
            hour=0,
            minutes={INFINITY},
            seconds={0},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 2, 0, 0, 0)


class TestWalkSeconds:
    def test_start_from_reference(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_seconds(
            year=2020,
            month=1,
            day=1,
            hour=0,
            minute=0,
            seconds={INFINITY},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 1, 0, 0, 0)

    def test_iter_all_seconds(self) -> None:
        reference_date = datetime.datetime(2020, 1, 1, 0, 0, 0)
        date_gen = walk_seconds(
            year=2020,
            month=1,
            day=1,
            hour=1,
            minute=0,
            seconds={INFINITY},
            weekdays={0, 1, 2, 3, 4, 5, 6},
            reference_date=reference_date,
        )
        assert next(date_gen) == datetime.datetime(2020, 1, 1, 1, 0, 0)
